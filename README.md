# Polls Project

This is a project to exercise software architecture elements. Enjoy!

## Scope

Provide remote execution for complex math calculation:

- sum(a,b) # portela
- sub(a,b) # igor
- div(a,b) # abraao
- mul(a,b) # william
- mod(a,b) # marcelo
- pow(a,b) # savio
- square_root(a) #
- log(a,b) #
- bhaskara(a,b,c) # 

## Collaborators

Marcelo dos Anjos Caldas
André Portela
David Oliveira
William Teixeira
Abraão Brandão
Rafael Pantoja
Igor Gouveia
Jonatas Lopes
Arthur Pezzin
