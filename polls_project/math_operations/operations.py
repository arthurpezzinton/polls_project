from abc import ABC, abstractmethod

from polls_project.logging import OperationLogger


class Operation(ABC):
    @abstractmethod
    def compute(self, *args):
        pass

    def process(self, *args):
        result = self.compute(*args)
        class_name = self.__class__.__name__
        OperationLogger().log(class_name, args, result)
        return result
